import { Component } from '@angular/core';
import {PersonService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
title = 'CapCo Code Assessment';
name = 'Kartheek Pathuri';
}
