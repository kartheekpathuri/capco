import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Headers, Response } from '@angular/http';
import { PersonService } from '../app.service';

import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-capco-table',
  templateUrl: './capco-table.component.html',
  styleUrls: ['./capco-table.component.css']
})
export class CapcoTableComponent implements OnInit {
  title = 'app';
  employees: Array<any>;
  totalRec: number;
  page = 1;
  itemsPerPage = 50;
  private httpService: HttpClient;

  constructor(private serv: PersonService) {
    this.employees = new Array<any>();
  }

  onPost(id, status) {
    alert('ID:' + id + ' ' + 'Status:' + status.toUpperCase());

    // let postMessage = 'id=' + id + '&status=' + status;
    this.httpService.post('api/submit', id, status).subscribe(data => console.log(JSON.stringify(data)));
  }

  setStyle(status) {
    switch (status) {
      case 'read':
        return 'blue';
      case 'unread':
        return 'green';
      case 'expired':
        return 'red';
    }
  }

  ngOnInit() {
    this.loadEmployee();
  }

  private loadEmployee() {
    this
      .serv
      .getPeople()
      .subscribe((resp: Response) => {
        this.employees = resp.json();
        this.totalRec = this.employees.length;
        // console.log(this.totalRec);
        // console.log(this.page);
      });
  }

}
