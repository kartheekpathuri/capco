import { Injectable } from '@angular/core';

import {BrowserModule} from '@angular/platform-browser';
import {Http, Response, RequestOptions, Headers} from '@angular/http';

import 'rxjs';
import { Observable } from 'rxjs';
import { CommonModule } from '@angular/common';

@Injectable()

export class PersonService {
private path = './assets/sample_data.json';

constructor(private http: Http) { }
  getPeople(): Observable<Response> {
      return this.http.get(this.path);
  }
}
